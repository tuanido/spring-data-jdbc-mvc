
//Modal controller

$('#deleteModal').on('show.bs.modal', function(event) {
	var button = $(event.relatedTarget)
	var deleteUrl = button.data('deleteurl')
	var modal = $(this)
	modal.find('a').attr('href', deleteUrl);
});

//Change page size controller
$('#pageSizeSelect').on('change', function() {
	$('#pageSizeForm').submit();
});