<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 
<!DOCTYPE html>
<html>
    
	<jsp:include page="../fragments/header.jsp" />
    <body>
   
    	<div class="container">
    	
		    <c:choose>
				<c:when test="${productForm['new']}">
					<h1>Add product</h1>
				</c:when>
				<c:otherwise>
					<h1>Update product</h1>
				</c:otherwise>
			</c:choose>
			<br />
		
	    	<form:form class="form-horizontal" method="post" modelAttribute="productForm" action="${pageContext.request.contextPath}/products">
	    	
				<spring:bind path="description">
	  				<div class="form-group ${status.error ? 'has-error' : ''}">
						<div class="form-group">
							<form:label path="description" class="col-sm-2 control-label">Description: </form:label>
							<div class="col-sm-10">
								<form:input path="description" type="text" value="${productForm.description}"/>
								<form:errors path="description" class="control-label" />
							</div>
						</div>
					</div>
				</spring:bind>
				
				<spring:bind path="price">
	  				<div class="form-group ${status.error ? 'has-error' : ''}">
						<div class="form-group">
							<form:label path="price" class="col-sm-2 control-label">Price</form:label>
							<div class="col-sm-10">
								<form:input path="price" type="number" value="${productForm.price}"/>
								<form:errors path="price" class="control-label" />
							</div>
						</div>
					</div>
				</spring:bind>
				<c:if test="${productForm.id != null}">
					<form:hidden path="id" value="${productForm.id }"/>
				</c:if>
				<c:if test="${productForm.createdAt != null}">
					<fmt:formatDate value="${productForm.createdAt}" var="dateString" pattern="dd/MM/yyyy hh:mm:ss" />
					<form:hidden path="createdAt" value="${dateString }"/>
				</c:if>
						
			    <div class="modal-footer">
				    <a href="${pageContext.request.contextPath}/products" class="btn btn-default">Cancel</a>
				    <button type="submit" class="btn btn-primary">Save</button>
			    </div>
	      	</form:form>
      	</div>
		
    </body>
</html>

