<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 
<!DOCTYPE html>
<html>

	<jsp:include page="../fragments/header.jsp" />

    <body>
		
    	<div class="container">
    	
	    	<c:if test="${not empty msg}">
			    <div class="alert alert-${css} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" 
	                                aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<strong>${msg}</strong>
			    </div>
			</c:if>
			
			<h1>Products</h1>
			
	    	<form id="pageSizeForm" method="GET" class="form-inline" method="POST" action="${pageContext.request.contextPath}/products">
				<div class="form-group">
			  		<label for="sel1">Page size:</label>
					<select id="pageSizeSelect" class="form-control" name="pageSize">
						<c:forEach var="size" items="${pageSizes }">
							<c:if test="${pageSize != null && pageSize == size }">
							    <option value="${size }" selected="selected">${size }</option>
						    </c:if>
						    <c:if test="${pageSize == null || pageSize != size }">
						    	<option value="${size }">${size }</option>
						    </c:if>
						</c:forEach>
					</select>
				</div>
			</form>
			
			<div>
			    <table class="table table-striped">
			      	<thead>
				        <tr>
		          			<th>#</th>
				            <th>Description</th>
				            <th>Price</th>
				            <th></th>
				        </tr>
			      	</thead>
			      	
			      	<tbody>
						<c:forEach var="product" items="${page.content}">
							<tr>
					            <td><c:out value="${product.id }"/></td>
					            <td><c:out value="${product.description }"/></td>
					            <td><c:out value="${product.price }"/></td>
					            <td>
					                <a href="${pageContext.request.contextPath}/products/${product.id}/update"><span class="glyphicon glyphicon-pencil"  aria-hidden="true"></span></a>
					                <a data-toggle="modal" data-target="#deleteModal" data-deleteurl="${pageContext.request.contextPath}/products/${product.id }/delete"><span class="glyphicon glyphicon-remove"  aria-hidden="true"></a>
					            </td>
					        </tr>
						</c:forEach>
					</tbody>
			    </table>
			</div>
			<div class="row">
				<div class="col-md-2">
					
				</div>
				<div class="col-md-10">
					<nav>
					  <ul class="pagination">
					  	
							<c:if test="${page.number ==  0 }">
						    	<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
							</c:if>
							
							<c:if test="${page.number !=  0 }">
							    <li><a href="${pageContext.request.contextPath}/products?pageSize=${pageSize}&pageNum=${page.number - 1}" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
							</c:if>
					  	
							<c:forEach var="index" begin="1" end="${page.totalPages}">
								<c:if test="${index == page.number + 1 }">
		    						<li class="active"><a href="${pageContext.request.contextPath}/products?pageSize=${pageSize}&pageNum=${index - 1}"><c:out value="${index }"/></a></li>
		    					</c:if>
								<c:if test="${index != page.number + 1 }">
		    						<li><a href="${pageContext.request.contextPath}/products?pageSize=${pageSize}&pageNum=${index - 1}"><c:out value="${index }"/></a></li>
		    					</c:if>
								
						  	</c:forEach>
							
							<c:if test="${page.number ==  page.totalPages - 1 }">
							    <li class="disabled"><a aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>
							</c:if>
							
							<c:if test="${page.number !=  page.totalPages - 1 }">
							    <li><a href="${pageContext.request.contextPath}/products?pageSize=${pageSize}&pageNum=${page.number + 1}" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>
							</c:if>
					  </ul>
					</nav>
				</div>
			</div>
		</div>
		
		

		<!-- Modals -->
		
		<!-- Delete confirmation modal -->
		<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		         <h3 id="myModalLabel">Delete Confirmation</h3>
		      </div>
			    <div class="modal-body">
			        <p class="error-text">Are you sure you want to delete the user?</p>
			    </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <a class="btn btn-danger">Delete</a>
		      </div>
		    </div>
		  </div>
		</div>
		
		<jsp:include page="../fragments/footer.jsp" />
		
    </body>
</html>