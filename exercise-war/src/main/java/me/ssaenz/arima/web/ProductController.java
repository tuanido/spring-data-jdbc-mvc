package me.ssaenz.arima.web;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import me.ssaenz.arima.model.Product;
import me.ssaenz.arima.service.ProductService;
import me.ssaenz.arima.validation.ProductFormValidator;

@Controller
public class ProductController {

	private static final String DATE_FORMAT = "dd/MM/yyyy hh:mm:ss";
	private static final int DEFAULT_INITIAL_PAGE = 0;
	private static final int DEFAULT_PAGE_SIZE = 4;
	private static final List<Integer> PAGE_SIZES = Arrays.asList(2, 4, 6, 10);
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductFormValidator productValidator;
	
    
    @InitBinder("productForm")
    protected void initProductBinder(WebDataBinder binder) {
        
        binder.setValidator(productValidator);
    }
    
    @InitBinder
    protected void initDateBinder(WebDataBinder binder) {
        
    	SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
    
    @RequestMapping(value = {"/", "/products"}, method = RequestMethod.GET)
    public String showProducts(Model model, HttpServletRequest request, @RequestParam(required=false, name="pageSize") Integer pageSize, @RequestParam(required=false, name="pageNum") Integer pageNum) {
    	
    	if (pageSize == null) {
    		
    		pageSize = DEFAULT_PAGE_SIZE;
    	}
    	
    	if (pageNum == null){
    		
			pageNum = DEFAULT_INITIAL_PAGE;
		}
    	
    	request.getSession().setAttribute("pageSize", pageSize);
    	return showProducts(model, pageSize, pageNum);
    }
    
    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public String save(@ModelAttribute("productForm") @Validated Product product, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
    	
    	if (bindingResult.hasErrors()) {
    	
    		return "products/productForm";
    	}
    	else {
    		
    		redirectAttributes.addFlashAttribute("css", "success");
        	
    		if(product.isNew()){
    			
    		  redirectAttributes.addFlashAttribute("msg", "User added successfully!");
    		}
    		else{
    			
    		  redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
    		}
        	
        	productService.save(product);
        	return "redirect:/products";	
    	}
    }
    
    @RequestMapping(value = "/products/{id}/update", method = RequestMethod.GET)
    public String update(Model model, @PathVariable Long id) {
    	Product product = productService.findOne(id);
    	model.addAttribute("productForm", product);
    	return "products/productForm";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.GET)
    public String create (Model model) {
    	model.addAttribute("productForm", new Product());
    	return "products/productForm";
    }
    
    @RequestMapping(value = "/products/{id}/delete", method = RequestMethod.GET)
    public String delete (HttpServletRequest request, RedirectAttributes redirectAttributes, @PathVariable Long id) {
    	
    	if (id != null) {
    		
    		productService.deleteProduct (id);
    	}
    	
    	Integer pageSize = (Integer) request.getSession().getAttribute("pageSize");
    	redirectAttributes.addAttribute("pageSize", pageSize);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "User is deleted!");
		
    	return "redirect:/products";
    }
    
    private String showProducts(Model model, Integer pageSize, Integer pageNum) {
    	Page<Product> productsPage = productService.getProducts(pageSize, pageNum);
    	model.addAttribute("page", productsPage);
    	model.addAttribute("pageSizes", PAGE_SIZES);
        return "products/list";
    }

}
