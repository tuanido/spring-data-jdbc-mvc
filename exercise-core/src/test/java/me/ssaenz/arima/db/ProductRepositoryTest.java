package me.ssaenz.arima.db;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import me.ssaenz.arima.config.H2DatabaseConfig;
import me.ssaenz.arima.config.RepositoryConfig;
import me.ssaenz.arima.config.SpringConfig;
import me.ssaenz.arima.model.Product;
import me.ssaenz.arima.repository.ProductRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={H2DatabaseConfig.class, RepositoryConfig.class, SpringConfig.class, ProductRepository.class})
public class ProductRepositoryTest {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Test
	public void deleteTest () {
		Assert.assertNotNull(productRepository);
		long previousCount = productRepository.count();
		
		Product product = new Product();
		product.setId(1l);
		productRepository.delete(product);
		
		Assert.assertEquals(previousCount - 1, productRepository.count());
	}
	
	@Test
	public void findAllTest () {

		Assert.assertNotNull(productRepository);
		long resultSize = productRepository.count();
		
		Iterable<Product> products = productRepository.findAll();
		Iterator<Product> iterator = products.iterator();
		
		int productCount = 0;
		while (iterator.hasNext()) {
			Product product = iterator.next();
			Assert.assertNotNull(product);
			productCount ++;
		}
		
		Assert.assertEquals(resultSize, productCount);
	}
	
	@Test
	public void findByIdTest () {

		Assert.assertNotNull(productRepository);
		Product product = productRepository.findOne(2L);
		Assert.assertNotNull(product);
		
	}
	
	@Test
	public void existsTest () {

		Assert.assertNotNull(productRepository);
		Boolean exists = productRepository.exists(2L);
		Assert.assertTrue(exists);
		
	}
	
	@Test
	public void createTest () {

		Assert.assertNotNull(productRepository);
		Product product = new Product();
		product.setDescription("Un producto de prueba");
		product.setPrice(50.0d);
		
		Product savedProduct = productRepository.save(product);
		
		Product dbProduct = productRepository.findOne(savedProduct.getId());
		
		Assert.assertEquals(savedProduct.getId(), dbProduct.getId());
		Assert.assertEquals(savedProduct.getDescription(), dbProduct.getDescription());
		Assert.assertEquals(savedProduct.getPrice(), dbProduct.getPrice());
		Assert.assertEquals(savedProduct.getCreatedAt(), dbProduct.getCreatedAt());
		
	}

	
	@Test
	public void updateTest () {

		Assert.assertNotNull(productRepository);
		
		Product dbProduct = productRepository.findOne(2L);

		dbProduct.setDescription("Updated description");
		
		Product updatedProduct = productRepository.save(dbProduct);
		
		Assert.assertEquals(dbProduct.getId(), updatedProduct.getId());
		Assert.assertEquals(dbProduct.getDescription(), updatedProduct.getDescription());
		Assert.assertEquals(dbProduct.getPrice(), updatedProduct.getPrice());
		Assert.assertEquals(dbProduct.getCreatedAt(), updatedProduct.getCreatedAt());
		Assert.assertEquals(dbProduct.getUpdatedAt(), updatedProduct.getUpdatedAt());
		
	}
	
	@Test
	public void findAllPageableTest () {
		Pageable pageRequest = new PageRequest(0, 5);
		
		Page<Product> productPage = productRepository.findAll(pageRequest);

		logPage(productPage);
		
		Assert.assertNotNull(productPage);
		Assert.assertEquals(5, productPage.getNumberOfElements());
		Assert.assertEquals(0, productPage.getNumber());
		

		pageRequest = new PageRequest(1, 5);
		
		productPage = productRepository.findAll(pageRequest);

		logPage(productPage);
		
		Assert.assertNotNull(productPage);
		Assert.assertEquals(3, productPage.getTotalPages());
		Assert.assertEquals(5, productPage.getNumberOfElements());
		Assert.assertEquals(1, productPage.getNumber());

	}

	private void logPage(Page<Product> productPage) {
		System.out.println("getNumber: " + productPage.getNumber());
		System.out.println("getNumberOfElements: " + productPage.getNumberOfElements());
		System.out.println("getSize: " + productPage.getSize());
		System.out.println("getTotalElements: " + productPage.getTotalElements());
		System.out.println("getTotalPages: " + productPage.getTotalPages());
		System.out.println("getContent.size: " + productPage.getContent().size());
		System.out.println("getSort: " + productPage.getSort());
	}

}
