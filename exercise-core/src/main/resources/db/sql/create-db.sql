CREATE TABLE product (
  id 			IDENTITY PRIMARY KEY,
  description	VARCHAR(255),
  price			DOUBLE,
  created_at	TIMESTAMP,
  updated_at	TIMESTAMP
);