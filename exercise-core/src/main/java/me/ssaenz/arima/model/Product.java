package me.ssaenz.arima.model;

import org.springframework.data.domain.Persistable;

public class Product extends Auditable implements Persistable<Long>{
	

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String description;
	
	private Double price;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public boolean isNew() {
		return this.id == null;
	}
	

}
