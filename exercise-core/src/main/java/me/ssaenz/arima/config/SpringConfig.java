package me.ssaenz.arima.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;

@ComponentScan({"com.ssaenz.arima"})
@Configuration
public class SpringConfig {
	
	@Autowired
	DataSource dataSource;

	@Bean
	public JdbcOperations getJdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}

}
