package me.ssaenz.arima.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoryConfig {
	
	@Bean
	public Properties productQueries () {
		Map<String, String> queries = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;

			{
				put("count", "SELECT COUNT(*) FROM product");
				
				put("delete.byId", "DELETE FROM product WHERE id = ?");
				
				put("find.all", "SELECT * FROM product");
				
				put("find.byId", "SELECT * FROM product WHERE id = ?");
				
				put("create", "INSERT INTO product (description, price, created_at, updated_at) VALUES (?, ?, ?, ?)");
				
				put("update", "UPDATE product SET description = ?, price = ?, created_at = ?, updated_at = ? WHERE id = ?");
				
				put("find.all.paged", "SELECT * FROM product LIMIT ? OFFSET ?");
			}
		};
		Properties properties = new Properties();
		properties.putAll(queries);
		return properties;
		
	}

}
