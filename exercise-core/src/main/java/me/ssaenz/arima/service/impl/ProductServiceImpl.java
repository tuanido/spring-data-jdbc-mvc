package me.ssaenz.arima.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import me.ssaenz.arima.model.Product;
import me.ssaenz.arima.repository.ProductRepository;
import me.ssaenz.arima.service.ProductService;
import me.ssaenz.arima.util.Util;

	
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	
	@Override
	public Collection<Product> getProducts() {
		
		return Util.makeCollection(productRepository.findAll());
	}

	@Override
	public Page<Product> getProducts(Integer numRows, Integer pageNum) {
		
		return productRepository.findAll(new PageRequest(pageNum, numRows));
	}

	@Override
	public void deleteProduct(Long id) {
		productRepository.delete(id);
	}

	@Override
	public Product findOne(Long id) {
		return productRepository.findOne(id);
	}

	@Override
	public void save(Product product) {
		productRepository.save(product);
	}

}
