package me.ssaenz.arima.service;

import java.util.Collection;

import org.springframework.data.domain.Page;

import me.ssaenz.arima.model.Product;

public interface ProductService {

	Collection<Product> getProducts();

	Page<Product> getProducts(Integer numRows, Integer pageNum);

	void deleteProduct(Long id);

	Product findOne(Long id);

	void save(Product productForm);
	
}
