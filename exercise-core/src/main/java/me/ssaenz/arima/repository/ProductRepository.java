package me.ssaenz.arima.repository;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import me.ssaenz.arima.model.Product;

@Repository
public class ProductRepository implements PagingAndSortingRepository<Product, Long> {

	private static final Logger logger = LoggerFactory.getLogger(ProductRepository.class);
	
	@Autowired
	private JdbcOperations jdbcOperations;
	
	@Autowired Properties productQueries;

	@Override
	public long count() {
		logger.info("count");
		String countQuery = productQueries.getProperty("count");
		Integer count = jdbcOperations.queryForObject(countQuery, Integer.class);
		return count;
	}

	@Override
	public void delete(Long id) {
		String deleteSql = productQueries.getProperty("delete.byId");
		jdbcOperations.update(deleteSql, new Object[]{id});
	}

	@Override
	public void delete(Product product) {
		this.delete(product.getId());
	}

	@Override
	public void delete(Iterable<? extends Product> products) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void deleteAll() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean exists(Long id) {
		Boolean result = Boolean.FALSE;
		try {
			result = findOne(id) != null;
		} catch (DataAccessException ignore) {
			
		}
		return result;
	}

	@Override
	public Iterable<Product> findAll() {
		String findAllQuery = productQueries.getProperty("find.all");
		return jdbcOperations.query(findAllQuery, new BeanPropertyRowMapper<Product>(Product.class));
	}

	@Override
	public Iterable<Product> findAll(Iterable<Long> productIds) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Product findOne(Long id) {
		String findByIdQuery = productQueries.getProperty("find.byId");
		return jdbcOperations.queryForObject(findByIdQuery, new Object[]{id}, new BeanPropertyRowMapper<Product>(Product.class));
	}

	@Override
	public <S extends Product> S save(S product) {
		
		S result = null;
		
		if (product.getId() == null) {
			
			result = createProduct(product);
		} 
		else {
			
			result = updateProduct(product);
		}
		return result;
	}
	
	private <S extends Product> S createProduct (S product) {
		
		final String findByIdQuery = productQueries.getProperty("create");
		Timestamp createdAt = new Timestamp(System.currentTimeMillis());
		Object[] params = new Object[]{product.getDescription(),
									   product.getPrice(),
									   createdAt,
									   null};
		
		KeyHolder keyHolder = new GeneratedKeyHolder();

		PreparedStatementCreatorFactory pscf = new PreparedStatementCreatorFactory(findByIdQuery,
																				  new int[]{Types.VARCHAR, 
																						  	Types.DOUBLE, 
																						  	Types.TIMESTAMP, 
																						  	Types.TIMESTAMP});
		
		jdbcOperations.update(pscf.newPreparedStatementCreator(params), keyHolder);
		
		product.setId(keyHolder.getKey().longValue());
		product.setCreatedAt(new Date(createdAt.getTime()));
		
		return product;
	}
	
	private <S extends Product> S updateProduct (S product) {

		String findByIdQuery = productQueries.getProperty("update");
		Timestamp updatedAt = new Timestamp(System.currentTimeMillis());
		jdbcOperations.update(findByIdQuery, new Object[]{product.getDescription(), 
														  product.getPrice(), 
														  product.getCreatedAt(),
														  updatedAt,
														  product.getId()});
		product.setUpdatedAt(new Date(updatedAt.getTime()));
		
		return product;
	}

	@Override
	public <S extends Product> Iterable<S> save(Iterable<S> products) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterable<Product> findAll(Sort sort) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Page<Product> findAll(Pageable pageable) {
		
		String findAllPagedQuery = productQueries.getProperty("find.all.paged");
		
		List<Product> productsPage = jdbcOperations.query(findAllPagedQuery, new Object[]{pageable.getPageSize(), pageable.getOffset()}, new BeanPropertyRowMapper<Product>(Product.class)); 
		long count = count();
		return new PageImpl<Product>(productsPage, pageable, count);
	}

}
